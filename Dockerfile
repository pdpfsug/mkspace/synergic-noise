FROM python:3-slim-buster
ENV PYTHONUNBUFFERED 1
RUN apt update && apt upgrade -y
RUN apt install -y nodejs npm build-essential && rm -rf /var/lib/apt/lists/*
WORKDIR /app
COPY package.json /app
COPY package-lock.json /app
RUN npm install
CMD ["node","app.js"]
